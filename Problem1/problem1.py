#!/usr/bin/env python3
'''
If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.

Find the sum of all the multiples of 3 or 5 below 1000.
'''

def solutionCheck():
    sumValues = 0
    for i in range(1, 1000):
        if (i % 3 == 0) or (i % 5 == 0):
            sumValues += i

    return sumValues

def runTimeSolution():
    numIterations = 1000 // 15
    
    # The first set of 7 numbers, or until they intersect is 60 and consists of 7 numbers
    # Every following iteration increases each of these 7 numbers by 15, adding 105 to the previous sum for that set of 7 numbers
    # That then gets added to the running total
    
    runningSum = 3 + 5 + 6 + 9 + 10 + 12 + 15
    print(runningSum)
    previousVal = runningSum
    for i in range(1, numIterations):
        previousVal += 105
        runningSum += previousVal
        print(runningSum)



if __name__ == '__main__':

    runTimeSolution()
    print("\nThe sum of all the multiples of 3 or 5 below 1000 is " + str(solutionCheck()) + ".\n")
